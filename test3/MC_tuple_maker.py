import os

from Configurables import (
    DaVinci,
    MCDecayTreeTuple,
)


from DecayTreeTuple.Configuration import *

import sys
#sys.path.append(os.environ['CHARMWGPRODROOT']+'/productions/Lb2L0ll')

def mc_tuple_maker():
    """
        Return a sequence for producing ntuples of Ks->mumu
        in order to test certification instance

    """

 
    tool_list = [
        'MCTupleToolKinematic',
        'MCTupleToolHierarchy',
        'MCTupleToolPID',
        'MCTupleToolReconstructed'
    ]

    mctree = MCDecayTreeTuple('MCTuple')
    for tool in tool_list:  mctree.ToolList += [tool]
    
    mctree.Decay    = 'B_s0 ==> ^(tau- ==> ^pi- ^pi- ^pi+ ^nu_tau) ^mu+'
    mctree.Branches = {
                'B_s0': 'B_s0 ==> (tau- ==> pi- pi- pi+ nu_tau) mu+',
                'tau': 'B_s0 ==> ^(tau- ==> pi- pi- pi+ nu_tau) mu+',
                'mu': 'B_s0 ==> (tau- ==> pi- pi- pi+ nu_tau) ^mu+',
                'pin1': 'B_s0 ==> (tau- ==> ^pi- pi- pi+ nu_tau) mu+',
                'pin2': 'B_s0 ==> (tau- ==> pi- ^pi- pi+ nu_tau) mu+',
                'pip': 'B_s0 ==> (tau- ==> pi- pi- ^pi+ nu_tau) mu+',
                'nu': 'B_s0 ==> (tau- ==> pi- pi- pi+ ^nu_tau) mu+',
    }
    DaVinci().UserAlgorithms += [mctree]

    return mctree

mc_tuple_maker()

