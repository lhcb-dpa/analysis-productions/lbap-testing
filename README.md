# Repository for testing analysis productions

This is a dummy copy of the main [`AnalysisProductions`](https://gitlab.cern.ch/lhcb-datapkg/AnalysisProductions/) data package that is used for running tests against the LHCb-Certification instance of LHCbDIRAC.
The results of pipelines ran here can be found at: https://test-lb-analysis-productions.web.cern.ch/.

For more information see [`LbAnalysisProductions`](https://gitlab.cern.ch/lhcb-dpa/analysis-productions/LbAnalysisProductions).
