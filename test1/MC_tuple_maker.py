import os

from Configurables import (
    DaVinci,
    MCDecayTreeTuple,
)


from DecayTreeTuple.Configuration import *

import sys
#sys.path.append(os.environ['CHARMWGPRODROOT']+'/productions/Lb2L0ll')

def mc_tuple_maker():
    """
        Return a sequence for producing ntuples of Ks->mumu
        in order to test certification instance

    """

 
    tool_list = [
        'MCTupleToolKinematic',
        'MCTupleToolHierarchy',
        'MCTupleToolPID',
        'MCTupleToolReconstructed'
    ]

    mctree = MCDecayTreeTuple('MCTuple')
    for tool in tool_list:  mctree.ToolList += [tool]
    
    mctree.Decay    = 'KS0 ==> ^mu+ ^mu-'
    mctree.Branches = {                        
                'KS0' : 'KS0 ==>  mu+  mu-',
                'mup' : 'KS0 ==> ^mu+  mu-',
                'mum' : 'KS0 ==>  mu+ ^mu-',
    }
    DaVinci().UserAlgorithms += [mctree]

    return mctree

mc_tuple_maker()



DaVinci().TupleFile = "LBAP_TEST1.root"
