# General options to lead for data
from Configurables import DaVinci

DaVinci().InputType = 'DST'
DaVinci().DataType = '2011'
DaVinci().Simulation = True
DaVinci().Lumi = False
DaVinci().DDDBtag = 'dddb-20150724'
DaVinci().CondDBtag = 'sim-20160606-vc-mu100'

